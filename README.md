# Snake Game

Simple C++ Sfml Snake Game

## Demo 

![Snake Game Gif](https://media.giphy.com/media/37q38U9QdjwTopwuGO/giphy.gif)

## Compiling 

You need to have [sfml](https://www.sfml-dev.org) installed.  
Get it using your package manager or download from [here](https://www.sfml-dev.org/dowload.php)

Code::Blocks project file is included  
meson.build file is also included

Compile with Code::Blocks or [Meson Build System](https://mesonbuild.com/)
